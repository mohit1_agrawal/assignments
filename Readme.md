Ubuntu version:
18.09

git version:
git version 2.17.1

kdiff3 version:
Qt: 4.8.7
KDE Development Platform: 4.14.38
kdiff3: 0.9.98 (64 bit)

BASH version:
4.4.20(1)-release (x86_64-pc-linux-gnu)

fabric version:
Fabric 1.14.0
Paramiko 2.6.0

python version:
Python 2.7.15+
Python 3.6.8

pip version:
pip 19.2.1 from /home/mohit/.local/lib/python2.7/site-packages/pip (python 2.7)
pip 9.0.1 from /usr/lib/python3/dist-packages (python 3.6)

vim version:
VIM - Vi IMproved 8.0 (2016 Sep 12, compiled Jun 06 2019 17:31:41)

docker version:
Docker version 19.03.1, build 74b1e89
docker-compose version 1.24.1, build 4667896
docker-machine version 0.16.0, build 702c267f

Virtualbox version:
version 6.0.11
