def msg = 'Dear "$firstname $lastname",\nSo nice to meet you in $city.\nSee you in ${month},\n${signed}'

def binding = ["firstname":"Sam", "lastname":"Pullara", "city":"San Francisco", "month":"December", "signed":"Groovy-Dev"]
def binding2 = ["firstname":"Mohit", "lastname":"Agrawal", "city":"Jaipur", "month":"December", "signed":"Groovy-Dev"]

def engine = new groovy.text.SimpleTemplateEngine()
def templater = engine.createTemplate(msg).make(binding)

def template = engine.createTemplate(msg).make(binding2)

print templater
print template

//def f = new File('/home/mohit/groovyscripts/test.template')
//def engine = new groovy.text.GStringTemplateEngine()
//def template = engine.createTemplate(f).make(binding)
//println template