class MyGroovyTest
{
    def sayHello()
    {
        println "Hello from MyGroovyTest class"
    }
    static void main(args)
    {
        def mgt = new MyGroovyTest()
        mgt.sayHello()
    }
}