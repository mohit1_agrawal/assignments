//(1..10).stream().map {it*4 }.forEach{ println it }

def factorial_imperative(n)
{
    def fact = 1
    for (i in 1..n)
    {
        fact *= i 
    }
    fact
}

def factorial_functional(n)
{
    (1..n).inject(1) {fact,e -> fact * e}
}

println factorial_imperative(5)
println factorial_functional(5)


numbers = [1,2,3,4,5,6,7,8,9]
println numbers.collect {it *2}

//println numbers.findAll {it % 2 == 0}.collect {it *2}
//println numbers.findAll {it % 2 != 0}.collect {it *2}
//



//private parseJSON(String)
//{ 
//    def jsonSlurper = new JsonSlurper()
//    def reader
//    reader = new BufferedReader(new InputStreamReader(new FileInputStream("/home/mohit/Desktop/MOCK_DATA.json"),"UTF-8"));
//    data = jsonSlurper.parse(reader);
//    println data
//    return data
//}




























//def closures= []
//for(i in 1..3)
//{
//    closures << {println i}
//}
//for(closure in closures) {closure()}
//(1..3).each {i -> closures << {println i }}
//for(closure in closures) {closure()}
//















