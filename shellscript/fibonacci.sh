#! /bin/bash

function fibonacci()
{
	if [[ $1 == 0 || $1 == 1 ]]
	then
		echo $1
    else
    	echo $[`fibonacci $[$1-2]` + `fibonacci $[$1 - 1]` ]
    fi
}

read -p "Enter number of terms that you want to print : " number_of_terms

for (( count = 0; count <= number_of_terms; count++ ))
do
	fibonacci $count
done
