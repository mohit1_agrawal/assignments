#! /bin/bash
echo -e "choose your option \n 1.for add user \n 2. to make username sudo \n 3. delete user\n 4. to check user sudo or not \n 5. to exit \n " 
read choice

if test "$choice" = "1"
then
	read -p "enter username : " username
	read -sp "enter password : " password
	echo 
	grep "^$username" /etc/passwd >/dev/null
	if [ $? -eq 0 ]; then
		echo "$username exists"
		exit 1
	else
		pass=$(perl -e 'print crypt($ARGV[0], "password")' $password)
		sudo useradd -m -p $pass $username
		[ $? -eq 0 ] && echo "User has been added successfully!" || echo "failed"
	fi

elif test "$choice" = "2"
then
	read -p "enter username : " username
	read -p "do u want to make user sudo?(y/n): " permsudo
	if test "$permsudo" = "y"
	then 
		sudo usermod -aG sudo $username
	else

		echo "okay!"
	fi


elif test "$choice" = "3"
then
	read -p "do u want to delete any user? (y/n): " deleteuser
	if test "$deleteuser" = "y"
	then
		read -p "enter the username to delete: " deluser
		echo "delete the $deluser"
		read -p "are you sure? (y/n): " confirmation
		if test "$confirmation" = "y"
		then 
			sudo userdel $deluser
		fi
	else

		echo "okay!"
	fi

elif test "$choice" = "4"
then
	read -p "Enter username : " username
	id $username

elif test "$choice" = "5"
then
	echo "thank you for using"
	exit 1

else
	echo "choose right option"
fi
