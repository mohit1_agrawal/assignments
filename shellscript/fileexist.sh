#! /bin/bash
select choice in "to check a regular file." "to check a dir." "to check wheather empty or not." "to read the content of a txt file." "to exit" 
do
	#echo -e "\nEnter your choice:\n 1. to check a regular file.\n 2. to check a dir.\n 3. to check wheather empty or not.\n 4. to read the content of a txt file.\n 5. to exit\n " 
	#read choice
	case $choice in
		"to check a regular file." )
			echo -e "\nEnter the name of the file : \c"
			read file_name
			if [ -f $file_name ]
			then
				echo " $file_name exists"
			else
				echo " $file_name not found"
			fi;;

		"to check a dir." )
			echo -e "\nEnter the name of the directory : \c"
			read file_name
			if [ -d $file_name ]
			then
				echo " $file_name exists"
			else
				echo " $file_name not found"
			fi;;

		"to check wheather empty or not." )
			echo -e "\nEnter the name of the file : \c"
			read file_name
			if [ -s $file_name ]
			then
				echo " $file_name not empty"
			else
				echo " $file_name empty"
			fi;;

		"to read the content of a txt file." )
			read -p "Enter file name that you want to read : " file_name
			while IFS= read -r line
			do
				echo $line
			done < $file_name;;

		"to exit" )
			echo "okay"
			exit 1 ;;

		* )
			echo "Enter correct choice"
	esac
done