from fabric.api import *
from fabric.colors import red


env.user = 'itt'
env.hosts = ['itt']

    
@task              # hello world
def cmd(command="pwd"):
    run(command)

@task              # change directory and multiple commands
def list1():
    run("mkdir -p demo")
    with cd("demo/"):
        run("touch {1..5}.txt")
        run("ls")
    run("rm -r demo")


@task              # using parameters
def newuser(uname, admin):
    print ("Old user {}.\n{} is new admin now.".format(uname,admin))
  


@task
def welcome():
    run("echo Welcome to getting start with fabric")

@task
def hello(name="mohit"):
    print red("Hello %s!" % name)


@task
def cal(first=10,second=20):
    first=int(prompt("Enter first number to add"))
    second=int(prompt("Enter second number to add"))
    print("Answer is %s!" % str(first+second))

    
@task
def host_type():
    run('uname -s')

@task
def uptime():
    run("uptime")

@task
def copy():
    run('mkdir -p /home/itt/mynewfolder')
    put('first.py', '/home/itt/mynewfolder')


@task
def getfiles(f=''):
    get(f, '/home/mohit/Desktop')

